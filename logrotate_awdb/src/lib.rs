extern crate chrono;
extern crate glob;
use std::path::PathBuf;

use chrono::prelude::*;
use chrono::{Duration, NaiveDate};
use glob::glob;

pub struct Log<'a> {
    pub logname: &'a String,
    pub logpath: &'a str,
}

impl<'a> Log<'a> {
    pub fn log_with_date(&self, a: i64) -> String {
        let dt = Local::now();
        let yesterday_dt = |y, m, d| NaiveDate::from_ymd(y, m, d) + Duration::days(a);

        self.logname.to_string()
            + &yesterday_dt(dt.year(), dt.month(), dt.day())
                .format(".%Y-%m-%d")
                .to_string()
    }
}

pub fn globdir(logpath: &str, logname: String) -> Vec<PathBuf> {
    let mut list = Vec::new();
    for entry in
        glob(&(logpath.to_string() + &logname.to_string() + "*")).expect("Cannot match pattern")
    {
        let entry = entry.unwrap();
        list.push(entry);
    }
    list
}
