mod lib;
use std::process::Command;
use std::{env, fs};

fn get_data(name_log: &String, path: &str) {
    let file = lib::Log {
        logname: &String::from(name_log),
        logpath: path,
    };

    let filename = Some(&file.logname as &str);
    let filedate = Some(file.log_with_date(-1));

    let files = lib::globdir(file.logpath, file.logname.to_string());
    for f in files.into_iter() {
        if f.file_name().unwrap().to_str() == filename {
            continue;
        } else if f.file_name().unwrap().to_str() == Some(filedate.as_ref().unwrap()) {
            Command::new("gzip")
                .arg("-9")
                .arg(f)
                .output()
                .expect("Cannot compress file");
        } else {
            fs::remove_file(f).expect("Cannot remove file");
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        eprint!("Use: <Command> <log name> <log path>");
        std::process::exit(1);
    } else {
        let name_log = env::args().nth(1).expect("Input error: log name");
        let path = env::args().nth(2).expect("Input error: log path");

        get_data(&name_log.to_string(), &path);
    }
}
